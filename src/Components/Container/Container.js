import React from 'react';
import './Container.css';
import Table from '../Table/Table';
import GraphContainer from '../GraphContainer/GraphContainer';
import TopNavBar from '../TopNavBar/TopNavBar';
import { LocalizedString as strings } from '../../Utils/LocalizedString';
import MetricsData from "../../Assets/metrics.json";

class Container extends React.Component {

  state = {
    activeComponent: 0,
    title: ""
  };

  onClickNavItem = (activeComponent, title) => {
    this.setState({ activeComponent, title });
  }

  render() {
    return (
      <>
        <TopNavBar onClickNavItem={this.onClickNavItem} />
        <h1 id="title">{this.state.title === "" ? strings.title.table : this.state.title}</h1>
        {/* Table */}
        {this.state.activeComponent === 0 && <Table data={MetricsData} />}
        {/* Network Tab */}
        {this.state.activeComponent === 1 &&
          <GraphContainer
            data={MetricsData}
            xLabel={strings.label.time}
            dataXAxis="time"
            positionXLabel={strings.positionLabel.bottom}
            positionYLabel={strings.positionLabel.insideBottomLeft}
            activeComponent={this.state.activeComponent}
          />}
        {/* Memory Usage Tab */}
        {this.state.activeComponent === 2 &&
          <GraphContainer
            data={MetricsData}
            dataXAxis={strings.fields.time}
            xLabel={strings.label.time}
            positionXLabel={strings.positionLabel.bottom}
            positionYLabel={strings.positionLabel.insideBottomLeft}
            activeComponent={this.state.activeComponent}
          />}
        {/* CPU Usage Tab */}
        {this.state.activeComponent === 3 &&
          <GraphContainer
            data={MetricsData}
            dataXAxis={strings.fields.time}
            xLabel={strings.label.time}
            positionXLabel={strings.positionLabel.bottom}
            positionYLabel={strings.positionLabel.insideBottomLeft}
            activeComponent={this.state.activeComponent}
          />}
        {/* DISK Tab */}
        {this.state.activeComponent === 4 &&
          <GraphContainer
            data={MetricsData}
            dataXAxis={strings.fields.time}
            xLabel={strings.label.time}
            positionXLabel={strings.positionLabel.bottom}
            positionYLabel={strings.positionLabel.insideBottomLeft}
            activeComponent={this.state.activeComponent}
          />}
        {/* Load Average Tab */}
        {this.state.activeComponent === 5 &&
          <GraphContainer
            data={MetricsData}
            dataXAxis={strings.fields.time}
            xLabel={strings.label.time}
            positionXLabel={strings.positionLabel.bottom}
            positionYLabel={strings.positionLabel.insideBottomLeft}
            activeComponent={this.state.activeComponent}
          />}
      </>
    );
  }
}

export default Container;
