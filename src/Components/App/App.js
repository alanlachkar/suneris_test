import React from 'react';
import './App.css';
import TopBanner from '../TopBanner/TopBanner';
import Container from '../Container/Container';

function App() {
  return (
    <div className="App">
      <TopBanner />
      <Container />
    </div>
  );
}

export default App;
