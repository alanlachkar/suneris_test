import React from 'react';
import './TopBanner.css';
import logo from "../../Assets/logo.png";

function TopBanner() {
  return (
    <header className="container">
      <img src={logo} className="App-logo" alt="logo" />
    </header>
  );
}

export default TopBanner;