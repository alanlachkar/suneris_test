import React from 'react';
import './Table.css';
import RenderRow from './RenderRow/RenderRow';
import { modifyArrayIndex } from '../../Utils/Utils';
import PropTypes from 'prop-types';

const originalTimeFieldIndex = 14;

class Table extends React.Component {

  getKeys = () => {
    var originalKeysList = Object.keys(this.props.data[0]);
    var modifiedKeysList = modifyArrayIndex(originalKeysList, originalTimeFieldIndex);
    return modifiedKeysList;
  }

  getTableHeader = () => {
    return this.getKeys().map((key, index) => {
      return <th key={index}>{key.toUpperCase()}</th>
    });
  }

  getRowsData = function () {
    var items = this.props.data;
    var keys = this.getKeys();
    return items.map((row, index) => {
      return <tr key={index}>
        <RenderRow data={row} keys={keys} />
      </tr>
    })
  }

  render() {
    return (
      <table id="elements">
        <tbody>
          <tr>{this.getTableHeader()}</tr>
          {this.getRowsData()}
        </tbody>
      </table>
    );
  }
}

Table.propTypes = {
  data: PropTypes.array.isRequired
}


export default Table;